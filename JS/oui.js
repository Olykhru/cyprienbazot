
var side = 0, //Variable retenant la valeur du coté affiché
	sides = $('.sides'),
	transform = [
		'rotate3d(1,0,0,-90deg)',
		'rotate3d(0,1,0,180deg)',
		'rotate3d(0,1,0,90deg)',
		'rotate3d(0,1,0,0deg)',
		'rotate3d(0,1,0,-90deg)',
		'rotate3d(1,0,0,90deg)'],
		//Paramètres de rotate3d de chaque coté dans l'ordre de numérotation des cotés
	translate = [
		'rotateX(90deg) translateZ(',
	 	'rotateY(180deg) translateZ(',
	 	'rotateY(-90deg) translateZ(',
	 	'translateZ(',
	 	'rotateY(90deg) translateZ(',
	 	'rotateX(-90deg) translateZ('],
		//Paramètres de rotate et translate de chaque coté dans l'ordre de numérotation des cotés
	detail = new Array($('.detail').length), //Variable servant à connaitre la taille "auto" des .detail pour les animations
	menu = $('.chgmtPage'),
	oHeight = $(window).outerHeight(),
	oWidth = $(window).outerWidth(),
	max = Math.max(oHeight, oWidth), //Variable permettant de rendre chaque coté carré tout en utilisant toute la place de l'écran
	min = Math.min(oHeight, oWidth), //Variable utile pour calculer les paddings haut et bas des cotés
	landscape, //Variable renseignant sur l'orientation du navigateur ainsi que sa largeur
	ecart = max - min,
	padding = ecart + (0.05 * max), //Variable renseignant les paddings haut et bas des sides, ou droite et gauche en fonction de quelle dimension est la plus grande
	content,
	scrollEnable = false, //Vaut true si la barre de scroll est affichée
	scrollAnimEnded = true,
	scrollMax,
	scrollMul,
	scrollSide = new Array(5),
	scrollVal,
	a,
	i,
	toDev,
	toMenu,
	toMoon,
	toScroll,
	toScrollBar,
	toTag,
	toText = new Array($('.content').length),
	toTrack,
	toTrans,
	touchScrollY,
	touchScrollY0,
	trackHeight;

function nightMode()
{
	$('html').toggleClass('night');
	$('body').toggleClass('night');
	$('h1').toggleClass('night');
	$('h2').toggleClass('night');
	$('hr').toggleClass('night');
	$('#chargement').toggleClass('night');
	$('#mode').toggleClass('night');
	$('#sun').toggleClass('night');
	$('.sides').toggleClass('night');
	$('#sign').toggleClass('night');
	$('#fleche').toggleClass('night');
	$('.hidden').toggleClass('night');
	$('.shown').toggleClass('night');
	$('.detail').toggleClass('night');
	$('#mentions').toggleClass('night');
	$('#menu').toggleClass('night');
	$('#toggleMenu').toggleClass('night');
	$('#hamburger').toggleClass('night');
	$('.chgmtPage').toggleClass('night');
	$('.cookies').toggleClass('night');
	$('#scrollBar').toggleClass('night');

	if($('#mode').hasClass('night'))
	{
		clearTimeout(toMoon);
		$('#moon').css({
			'display' : 'block',
			'opacity' : '0',
			'-webkit-transition' : 'none',
			'-moz-transition' : 'none',
			'transition' : 'none',
			'right' : landscape ? '-118px' : '-120%'
		});
		setTimeout(function()
		{
			$('#moon').css({
				'filter' : 'drop-shadow(0 0 10px #ccc)',
				'opacity' : '1',
				'-webkit-transition' : 'all 2s ease-in-out, filter 0.5s 1.5s linear',
				'-moz-transition' : 'all 2s ease-in-out, filter 0.5s 1.5s linear',
				'transition' : 'all 2s ease-in-out, filter 0.5s 1.5s linear',
				'right' : landscape ? '-4.8vmin' : '-51%'
			});
		}, 10);

		if(getCookie('cookiesEnable') == 'oui')
		{
			let expires = new Date(new Date().getTime + (365.25 * 24 * 3600 * 1000));
			document.cookie = 'night=1; expires=' + expires +'; path=/;';
		}
	}
	else
	{
		$('#moon').css({
			'filter' : 'drop-shadow(0 0 0px #ccc)',
			'opacity' : '0',
			'right' : landscape ? '2.2vmin' : '20%'
		});
		toMoon = setTimeout(function()
		{
			$('#moon').css('display', 'none');
		}, 2000);

		if(getCookie('cookiesEnable') == 'oui')
		{
			let expires = new Date(new Date().getTime + (365.25 * 24 * 3600 * 1000));
			document.cookie = 'night=0; expires=' + expires +'; path=/;';
		}
	}

	togglescroll(side);
}

function togglescroll(face)
{
	$('#scrollBar').css('height', $(window).height() + 'px');

	if(side !== 0) //Parce qu'il n'y a pas de '.content' sur le #side1
	{
		content = $('.content')[face - 1]; //'side - 1' également parce qu'il n'y a pas de '.content' sur le #side1

		if(content.scrollHeight > ($(content).outerHeight() + 7)) //Je ne trouve pas pourquoi quand le content n'est pas à sa taille max, la différence entre ces deux valeurs est de 7 (et de 4 sans le padding), j'ai donc ajouter ce '+7' pour ne pas afficher une ombre et une barre de défilement inutiles
		{
			scrollEnable = true;
			scrollMax = content.scrollHeight - $(content).outerHeight();
			trackHeight = scrollMax > (parseFloat($('#scrollBar').css('height')) - 30);
			scrollMul = trackHeight ? scrollMax / (oHeight - 30) : 1;

			scrollSide[face - 1] = trackHeight ? content.scrollTop / scrollMul : $('#scrollTrack').css('top');

			if(!$(content).hasClass('portfolio'))
			{
				$(content).css('justify-content', 'flex-start');
			}

			$('#scrollTrack').css({
				'height' : trackHeight ? '30px' : (oHeight - scrollMax) + 'px',
				'top' : scrollSide[face - 1]
			});
			$('#scrollBar').css({
				'width' : '10px',
				'opacity' : '1'
			});

			clearTimeout(toScrollBar);
			$('#scrollTrack').trigger('change');
		}
		else
		{
			scrollEnable = false;
			$(content).css('justify-content', 'center');
			$(content).removeClass('scrollHaut').removeClass('scrollMilieu').removeClass('scrollBas');
			$('#scrollBar').css('opacity', '0');
			$('#scrollTrack').css('top', '0');

			scrollSide[face - 1] = 0

			toScrollBar = setTimeout(function()
			{
				$('#scrollBar').css('width', '0');
			}, 1000);
			scrollMax = 0;
		}
	}
	else
	{
		scrollEnable = false;
		$('#scrollBar').css('opacity', '0');
		toScrollBar = setTimeout(function()
		{
			$('#scrollBar').css('width', '0');
		}, 1000);
		scrollMax = 0;
	}
}

function getCookie(cname)
{
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ')
		{
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0)
		{
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

if(navigator.cookieEnabled)
{
	if(getCookie('cookiesEnable') == 'oui')
	{
		$('#cookie').css('display', 'none');
	}
	else //Si ce cookie est différent de oui alors il n'existe pas (à moins d'avoir été modifier dans la console évidemment), sa valeur est donc définie à 'oui' pour permettre de changer de theme et de le garder en mémoire avant d'avoir accepté les cookies. Les cookies gardant en mémoire le thème, ainsi que celui là, seront supprimés si les cookies sont refusés
	{
		let expires = new Date(new Date().getTime + (60 * 2 * 1000));
		document.cookie = 'cookiesEnable=oui; expires=' + expires +'; path=/;';
	}

	if(getCookie('night') === '')
	{
		let expires = new Date(new Date().getTime + (365.25 * 24 * 3600 * 1000));
		document.cookie = 'night=0; expires=' + expires +'; path=/;';
	}
	else if(getCookie('night') == '1')
	{
		nightMode();
	}
}
else   //Si les cookies ne sont pas activés sur le navigateur, autant ne pas demander si l'utilisateur les accepte
{
	$('#cookie').css('display', 'none');
}

$(document).ready(function()
{
	//Petit chargement qui met bien
	setTimeout(function()
	{
		$('#chargement').css('opacity', '0');
	}, 3000);
	setTimeout(function()
	{
		$('#chargement').css('display', 'none');
	}, 4000);

	//Les fonctions
	function dragElement(elmnt)
	{
	  var pos1 = 0,
			pos2 = 0;
	  $(elmnt).on('mousedown', dragMouseDown);
		$(elmnt).on('touchstart', dragMouseDown);

	  function dragMouseDown(e)
		{
	    e = e || window.event;
	    e.preventDefault();
	    pos2 = e.clientY;

	    document.onmouseup = closeDragElement;
	    document.onmousemove = elementDrag;
			document.ontouchend = closeDragElement;
			document.ontouchmove = elementDrag;
	  }

	  function elementDrag(e)
		{
	    e = e || window.event;
	    e.preventDefault();

	    pos1 = pos2 - e.clientY;
	    pos2 = e.clientY;

	    $(elmnt).css('top', (elmnt.offsetTop - pos1) + "px");

			$(elmnt).trigger('change');
	  }

	  function closeDragElement()
		{
	   	document.onmouseup = null;
	   	document.onmousemove = null;
	  }
	}

	function trans() //Met à jour la barre de scroll pendant que la page s'affiche et fini sa transition
	{
		togglescroll(side);
		toTrans = setTimeout(function()
		{
			trans();
		}, 5);
	}

	function redimension()
	{
		oHeight = $(window).outerHeight();
		oWidth = $(window).outerWidth();
		landscape = (oWidth > oHeight) && (oWidth > 900);
		max = Math.max(oHeight, oWidth);
		min = Math.min(oHeight, oWidth);
		ecart = max - min;
		padding = ecart + (0.05 * max);

		for(i = 0; i < $('.detail').length; i++)
		{
			$($('.detail')[i]).css({'-webkit-transition' : 'height 0s ease-in-out',
					'-moz-transition' : 'height 0s ease-in-out',
					'-o-transition' : 'height 0s ease-in-out',
					'transition' : 'height 0s ease-in-out',
			});
			$($('.detail')[i]).css('height', 'auto');

			detail[i] = $($('.detail')[i]).css('height');

			if($($('.dev')[i]).hasClass('hidden'))
			{
				$($('.detail')[i]).css('height', 0);
			}

			$($('.detail')[i]).css({'-webkit-transition' : 'height 0.5s ease-in-out',
					'-moz-transition' : 'height 0.5s ease-in-out',
					'-o-transition' : 'height 0.5s ease-in-out',
					'transition' : 'height 0.5s ease-in-out'
			});

			$($('.detail')[i]).on('transitionrun', function()
			{
				clearTimeout(toTrans);
				trans();
			});
			$($('.detail')[i]).on('transitionend', function()
			{
				clearTimeout(toTrans);
				redimension();
			});
			$($('.detail')[i]).on('transitioncancel', function()
			{
				clearTimeout(toTrans);
				redimension();
			});
		}

		for(i = 0; i < 6; i++)
		{
			$(sides[i]).css(
				{
					'height' : max + 'px',
			  	'width' : max + 'px',
					'padding' : landscape ?
												(max == oHeight ?
													('5% ' + (ecart / 2) + 'px 72px') : /* 72 == 75 (taille du menu) - 3 (taille de la border) (pareil en dessous évidemment) */
													((padding / 2) + 'px 0 ' + (ecart / 2 + 72) + 'px')
												) :
												(max == oHeight ?
													('5% ' + (ecart / 2) + 'px 0') :
													((padding / 2) + 'px 0 ' + (ecart / 2) + 'px')
												),
					'transform': translate[i] + (max / 2) + 'px',
					'-moz-transform': translate[i] + (max / 2) + 'px',
					'-webkit-transform': translate[i] + (max / 2) + 'px'
				});
		};

		$('#D3Cube').css(
		{
			'height' : max + 'px',
			'width' : max + 'px',
			'transform' : transform[side]
										+ ' translate'
										+ (max == oHeight ?
												(side == 0 ? 'X(-' :
													(side == 1 ? 'X(' :
														(side == 2 ? 'Z(-' :
															(side == 3 ? 'X(-' :
																(side == 4 ? 'Z(' :
																	'X(-'))))) :
												(side == 0 ? 'Z(-' :
													(side == 1 ? 'Y(-' :
														(side == 2 ? 'Y(-' :
															(side == 3 ? 'Y(-' :
																(side == 4 ? 'Y(-' :
																	'Z('))))))
										+ (ecart / 2)
										+ 'px)',
			'transition' : 'none'
		});

		$('#menu').css('height', landscape ? '7.5vmin' : 'auto');

		$('#toggleMenu').css({'-webkit-transition' : 'all 0s ease-in-out',
				 									'-moz-transition' : 'all 0s ease-in-out',
					 								'-o-transition' : 'all 0s ease-in-out',
													'transition' : 'all 0s ease-in-out',
													'width' : $('#toggleMenu').css('height')
		});
		$('#toggleMenu').css({'-webkit-transition' : 'all 0.3s ease-in-out',
				 									'-moz-transition' : 'all 0.3s ease-in-out',
					 								'-o-transition' : 'all 0.3s ease-in-out',
													'transition' : 'all 0.3s ease-in-out'
		});

		let lastDetailImgs = $('.detail')[$('.detail').length - 1].getElementsByTagName('img');
		for(i = 0; i < lastDetailImgs.length; i++)
		{
			$(lastDetailImgs[i]).css('max-height', (0.8 * $($('.content')[side - 1]).innerHeight()));
		}

		if($('#oui').length > 0)
		{
			$('#oui').css('height', max);
		}

		togglescroll(side);
		$('#D3Cube').css('transition', 'all 3s ease-in-out');
	};

	function rotation(face)
	{
		$(menu[side]).removeClass('pageActive');
		$('#mentions').removeClass('visible');

		switch (face)
		{
			case 0:
				side = 0;
				let translate0 = ' translate' + (max == oHeight ? 'X(-' : 'Z(-') + (ecart / 2) + 'px)';
				$('#D3Cube').css({'transform' : 'rotate3d(1,0,0,-90deg)' + translate0,
									'-webkit-transform' : 'rotate3d(1,0,0,-90deg)' + translate0,
									'-moz-transform' : 'rotate3d(1,0,0,-90deg)' + translate0,
									'-o-transform' : 'rotate3d(1,0,0,-90deg)' + translate0});
				$('#menu').css('opacity', '0');
				toMenu = setTimeout(function()
				{
					$('#menu').css('height', '0');
				}, 1000);
				break;

			case 1:
				if(side == 0) {
					$('#menu').css('opacity', '1');
					$('#menu').css('height', landscape ? '7.5vmin' : 'auto');
				};
				side = 1;
				let translate1 = ' translate' + (max == oHeight ? 'X(' : 'Y(-') + (ecart / 2) + 'px)';
				$('#D3Cube').css({'transform' : 'rotate3d(0,1,0,180deg)' + translate1,
									'-webkit-transform' : 'rotate3d(0,1,0,180deg)' + translate1,
									'-moz-transform' : 'rotate3d(0,1,0,180deg)' + translate1,
									'-o-transform' : 'rotate3d(0,1,0,180deg)' + translate1});
				break;

			case 2:
				if(side == 0) {
					$('#menu').css('opacity', '1');
					$('#menu').css('height', landscape ? '7.5vmin' : 'auto');
				};
				side = 2;
				let translate2 = ' translate' + (max == oHeight ? 'Z(-' : 'Y(-') + (ecart / 2) + 'px)';
				$('#D3Cube').css({'transform' : 'rotate3d(0,1,0,90deg)' + translate2,
									'-webkit-transform' : 'rotate3d(0,1,0,90deg)' + translate2,
									'-moz-transform' : 'rotate3d(0,1,0,90deg)' + translate2,
									'-o-transform' : 'rotate3d(0,1,0,90deg)' + translate2});
				break;

			case 3:
				if(side == 0) {
					$('#menu').css('opacity', '1');
					$('#menu').css('height', landscape ? '7.5vmin' : 'auto');
				};
				side = 3;
				let translate3 = ' translate' + (max == oHeight ? 'X(-' : 'Y(-') + (ecart / 2) + 'px)';
				$('#D3Cube').css({'transform' : 'rotate3d(0,1,0,0deg)' + translate3,
									'-webkit-transform' : 'rotate3d(0,1,0,0deg)' + translate3,
									'-moz-transform' : 'rotate3d(0,1,0,0deg)' + translate3,
									'-o-transform' : 'rotate3d(0,1,0,0deg)' + translate3});
				break;

			case 4:
				if(side == 0) {
					$('#menu').css('opacity', '1');
					$('#menu').css('height', landscape ? '7.5vmin' : 'auto');
				};
				side = 4;
				let translate4 = ' translate' + (max == oHeight ? 'Z(' : 'Y(-') + (ecart / 2) + 'px)';
				$('#D3Cube').css({'transform' : 'rotate3d(0,1,0,-90deg)' + translate4,
									'-webkit-transform' : 'rotate3d(0,1,0,-90deg)' + translate4,
									'-moz-transform' : 'rotate3d(0,1,0,-90deg)' + translate4,
									'-o-transform' : 'rotate3d(0,1,0,-90deg)' + translate4});
				break;

			case 5:
				if(side == 0) {
					$('#menu').css('opacity', '1');
					$('#menu').css('height', landscape ? '7.5vmin' : 'auto');
				};
				side = 5;
				let translate5 = ' translate' + (max == oHeight ? 'X(-' : 'Z(') + (ecart / 2) + 'px)';
				$('#D3Cube').css({'transform' : 'rotate3d(1,0,0,90deg)' + translate5,
									'-webkit-transform' : 'rotate3d(1,0,0,90deg)' + translate5,
									'-moz-transform' : 'rotate3d(1,0,0,90deg)' + translate5,
									'-o-transform' : 'rotate3d(1,0,0,90deg)' + translate5});
				break;
		}

		if(side != 0)
		{
			clearTimeout(toMenu);
		}

		$(menu[side]).addClass('pageActive');

		if(!landscape && $('#menu').hasClass('oui'))
		{
			$('#hamburger').toggleClass('cross');
			$('#menu').toggleClass('oui');
		}

		togglescroll(face);
		setTimeout(function()
		{
			togglescroll(face); //Pourquoi il faut le faire ?? Je sais pas vraiment, je sais juste que sinon le scroll ne va pas jusqu'en bas, il n'affiche pas la dernière flèche si il y en a une à la fin (la première fois où l'on va sur la page)
		}, 200);
	}

	function scrollBgImg(face)
	{
		if(scrollEnable)
		{
			clearTimeout(toScroll);
			scrollAnimEnded = false;
			$('#scrollTrack').css('top', ($($('.content')[side - 1]).scrollTop() / scrollMax) * ($('#scrollBar').height() - $('#scrollTrack').height()));

			Math.floor(parseFloat($('#scrollTrack').css('top'))) == 0 ?
					$($('.content')[face - 1]).addClass('scrollHaut').removeClass('scrollMilieu').removeClass('scrollBas') :
					(Math.ceil(parseFloat($('#scrollTrack').css('top'))) >= scrollMax / (trackHeight ? scrollMul : 1) ?
							$($('.content')[face - 1]).addClass('scrollBas').removeClass('scrollMilieu').removeClass('scrollHaut') :
					$($('.content')[face - 1]).addClass('scrollMilieu').removeClass('scrollBas').removeClass('scrollHaut'));

			scrollAnimEnded = true;
		}
	}

	//Affichage du dessus du cube au chargement de la page
	rotation(0);

	//Transformation de chacun des cotés en carré adapté à la taille de l'écran
	redimension();

	dragElement(document.getElementById('scrollTrack'));

	//EventListeners
	$('#il').click(function(i) {
		rotation(0);
	});
	$('#i').click(function() {
		rotation(1);
	});
	$('#sam').click(function() {
		rotation(2);
	});
	$('#sa').click(function() {
		rotation(3);
	});
	$('#o').click(function() {
		rotation(4);
	});
	$('#yuk').click(function() {
		rotation(5);
	});
	$($('td')[1]).click(function()
	{
		if($('#oui').length === 0)
		{
			let oui = document.createElement('img');
			$(oui).attr({
				'id' : 'oui',
				'src' : 'IMG/JEsperePasRéelementFairePeur.png'
			});
			$(oui).css({
				'height' : max,
				'opacity' : '0.5',
				'position' : 'absolute',
				'top' : '0',
				'z-index' : '-1'
			});
			$('#side1').append($(oui));
			$('#side1').css({
				'-webkit-backface-visibility' : 'visible',
				   '-moz-backface-visibility' : 'visible',
								'backface-visibility' : 'visible'
			}); //Pour charger avant d'être visible
			setTimeout(function()
			{
				$('#side1').css({
					'-webkit-backface-visibility' : 'hidden',
					   '-moz-backface-visibility' : 'hidden',
									'backface-visibility' : 'hidden'
				});
			}, 50);
		}
		redimension();
	});
	$('#fleche').click(function() {
		rotation(1);
	});

	$('#toggleMenu').click(function()
	{
		$('#hamburger').toggleClass('cross');
		$('#menu').toggleClass('oui');
	});
	$('#toggleMentions').click(function()
	{
		$('#mentions').addClass('visible');
		setTimeout(function()
		{
			document.onclick = function(event)
			{
				if(event.path[0].id != 'mentions' && event.path[1].id != 'mentions')
				{
					$('#mentions').removeClass('visible');
					document.onclick = null;
				}
			}
		}, 10);
	});

	$('#cookiesOui').click(function()
	{
		$('#cookie').css('opacity', '0');
		setTimeout(function()
		{
			$('#cookie').css('display', 'none');
		}, 1000);

		let expires = new Date(new Date().getTime + (365.25 * 24 * 3600 * 1000));
		document.cookie = 'cookiesEnable=oui; expires=' + expires +'; path=/;';
	});
	$('#cookiesNon').click(function()
	{
		$('#cookie').css('opacity', '0');
		setTimeout(function()
		{
			$('#cookie').css('display', 'none');
		}, 1000);

		document.cookie = 'night=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;';
		document.cookie = 'cookiesEnable=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;';
	});

	$('#scrollTrack').on('change', function()
	{
		let top = parseFloat($('#scrollTrack').css('top'));
		$('#scrollTrack').css('top', top < 0 ? 0 : (top > scrollMax / (trackHeight ? scrollMul : 1) ?  scrollMax / (trackHeight ? scrollMul : 1) : top));

		$($('.content')[side - 1]).scrollTop(scrollMul * top);

		scrollBgImg(side);
	});
	$('#scrollTrack').on('scroll', function()
	{
		$('#scrollTrack').stop().animate({
			top : ((trackHeight ? scrollMul : 1) * scrollVal < 0 ? 0 : (scrollVal > scrollMax / (trackHeight ? scrollMul : 1) ? scrollMax / (trackHeight ? scrollMul : 1) : scrollVal))
		}, 0);

		let oui = scrollVal * scrollMul;
		$($('.content')[side - 1]).stop().animate({
			scrollTop : (oui < 0 ? 0 : (oui > scrollMax ? scrollMax : oui))
		}, 0);

		clearTimeout(toScroll);
		toScroll = setTimeout(function()
		{
			scrollBgImg(side);
		}, 10);
	});

	document.onkeydown = function(event) //Je trouve ça pratique même si personne ne sait que c'est possible, et qu'il y a des secousses si on change de page sur laquelle on se dirige en peine rotation (secousses qui ne sont pas présentent en cliquant sur le menu pendant la rotation)
	{
    event = event || window.event;
    if(event.key == 'Escape')
		{
      $('#mentions').removeClass('visible');
			document.onclick = null;
    }
		else if(/^[0-5]$/.test(event.key))
		{
			rotation(parseInt(event.key));
		}
	};


	document.addEventListener('touchstart', function(event)
	{
		touchScrollY = event.touches[0].clientY;
		touchScrollY0 = event.touches[0].clientY;

		$($('.content')[side - 1]).stop();
		$('#scrollTrack').stop();

		if(!$(event.target).is('#toggleMenu') && !($(event.target).hasClass('chgmtPage')) && !($(event.target).hasClass('hamburger')) && $('#hamburger').hasClass('cross'))
		{
			$('#hamburger').removeClass('cross');
			$('#menu').removeClass('oui');
		}
	});
	document.addEventListener('touchmove', function(event)
	{
		touchScrollY = touchScrollY0;
		touchScrollY0 = event.touches[0].clientY;

		if(touchScrollY0 < touchScrollY)
	  {
			scrollVal = (scrollAnimEnded ? parseFloat($('#scrollTrack').css('top')) : scrollVal) + (1 / scrollMul * (touchScrollY - touchScrollY0));
		}
		else
	  {
			scrollVal = (scrollAnimEnded ? parseFloat($('#scrollTrack').css('top')) : scrollVal) - (1 / scrollMul * (touchScrollY0 - touchScrollY));
		}

		$('#scrollTrack').css('top', ((trackHeight ? scrollMul : 1) * scrollVal < 0 ? 0 : (scrollVal > scrollMax / (trackHeight ? scrollMul : 1) ? scrollMax / (trackHeight ? scrollMul : 1) : scrollVal)));

		let oui = scrollVal * scrollMul;
		$($('.content')[side - 1]).scrollTop(oui < 0 ? 0 : (oui > scrollMax ? scrollMax : oui));

		scrollBgImg(side);
	});
	document.addEventListener('touchend', function()
	{
		let diffY = Math.abs(100 * (touchScrollY - touchScrollY0) / oHeight);

		if(diffY > 0.4)
		{
			scrollAnimEnded = false;
			let scrollVal0 = scrollVal;
			let i = 0;

			if(touchScrollY > touchScrollY0)
			{
				scrollVal = (scrollAnimEnded ? parseFloat($('#scrollTrack').css('top')) : scrollVal) + (1 / scrollMul * (diffY / 2 * oHeight));
			}
			else
			{
				scrollVal = (scrollAnimEnded ? parseFloat($('#scrollTrack').css('top')) : scrollVal) - (1 / scrollMul * (diffY / 2 * oHeight));
			}

			let delai = (touchScrollY - touchScrollY0); //Math.max(Math.abs(scrollVal - scrollVal0) / 3, 1500);

			$('#scrollTrack').stop().animate({'top' : ((trackHeight ? scrollMul : 1) * scrollVal < 0 ? 0 : (scrollVal > scrollMax / (trackHeight ? scrollMul : 1) ? scrollMax / (trackHeight ? scrollMul : 1) : scrollVal))}, delai);

			let oui = scrollVal * scrollMul;
			$($('.content')[side - 1]).stop().animate({scrollTop : oui < 0 ? 0 : (oui > scrollMax ? scrollMax : oui)}, 1500);

			clearTimeout(toScroll);
			toScroll = setTimeout(function()
			{
				scrollBgImg(side);
			}, 1510);
		}
	});

	document.addEventListener('wheel', function(event)
	{
		console.log(event.deltaY);
		let y = event.deltaY;

		if(y >= 1)
	  {
			scrollVal = (scrollAnimEnded ? parseFloat($('#scrollTrack').css('top')) : scrollVal) + (1 / scrollMul * 70);
		}
		else if(y <= 1)
	  {
			scrollVal = (scrollAnimEnded ? parseFloat($('#scrollTrack').css('top')) : scrollVal) - (1 / scrollMul * 70);
		}

		$('#scrollTrack').trigger('scroll');
	}, {
		passive : true
	});

	for(i = 0; i < $('.dev').length; i++)
	{
		$($('.dev')[i]).on('click', function()
		{
			let devID = $('.dev').index(this);
			$(this).toggleClass('hidden').toggleClass('shown');

			if($(this).hasClass('shown'))
			{
				$($('.detail')[devID]).css('height', detail[devID]);
			}
			else
			{
				$($('.detail')[devID]).css('height', 0);
				clearTimeout(toText[devID]);
			}

			clearTimeout(toDev);
			toDev = setTimeout(function()
			{
				togglescroll(side);
			}, 160);
		});
	}

	$('#mode').click(function()
	{
		nightMode();
	});

	//Centrage et redimension du cube en même temps que le redimensionnement de la page
	$(window).resize(function() {
		redimension();
	});
});
